# -*- coding: utf-8 -*-
"""last edited : 09/03/2021

Class contains:
Object Moultijunction creating stack of monojonction
--> example of an object creation : Cell = Multijunction(Eg_tab = Gap, T = Ta, spectrum = spectrum)
Eg_tab in eV, spectrum choices : "AM1.5D", "AM1.5G", "AM0G", "BB"

Functions:
--> PrintMe gives list of cell, gap, partial efficency and total efficiency
--> PrintDetails gives all parameters and calculated values of each monojunction
"""
###################################################
##Import des différentes bibliothèques à utiliser##
###################################################

from monojunction_1 import Monojunction

########################################
##Définition d'une class multijonction##
########################################

class Multijunction :

    ##Constructeur de la class##
    def __init__(self, Eg_tab, T, spectrum, dV, irradiation):
        
        # Attributs
        
        self.Eg_tab = sorted(Eg_tab) #Tableau contenant les différentes énergies de gaps trié par ordre croissant(eV)
        self.nb = len(self.Eg_tab) #Nombre de gaps
        self.T = T #Température de la cellule (K)
        self.spectrum = spectrum #Type de spectre solaire utilisé
        self.monojunction_tab = [] #Tableau contenant les caractéristiques des différentes monojonctions
        self.eff = .0 #Rendement
        self.Pabs = .0 #Puissance absorbé par la cellule et liée au rayonnement du soleil
        self.Pelec = .0 #Puissance électrique généré par la cellule
        self.dV = dV
        self.irradiation = irradiation

        #Paramétrage des attributs

        self.creaction() #Création des monojonctions associées aux différents gaps
        self.set_eff() #Calcul du rendement
        self.set_Pabs() #Calcul de la puissance absorbé
        self.set_Pelec() #Calcul de la puissance électrique
  
    ##Création des monojonctions##
    def creaction(self) :
        
        for index in range(self.nb) : #On parcourt les différents gaps de la cellule
            
            if index == self.nb - 1 : #Si c'est le dernier gap, on fixe une valeur de E_max tel que la puissance absorbé  est négligeable à partir de cette énergie
                Cell = Monojunction(Eg = self.Eg_tab[index], Emax = 10, T = self.T, spectrum = self.spectrum, dV=self.dV, irradiation = self.irradiation) #Définition de la monojonction
            
            else :
                Cell = Monojunction(Eg = self.Eg_tab[index], Emax = self.Eg_tab[index + 1], T = self.T, spectrum = self.spectrum, dV=self.dV, irradiation = self.irradiation) #Définition de la monojonction
                
            self.monojunction_tab.append(Cell) #On ajoute la monojonction défini au tableau des différentes monojonctions

    ##Calcul du rendement##   
    def set_eff(self) :
        for index in range(self.nb) : #On parcourt les différentes monojonctions
            self.eff = self.eff + self.monojunction_tab[index].eff #On somme le rendement de chaque monojonctions
        
    ##Calcul de la puissance absorbé##
    def set_Pabs(self) :
        for index in range(self.nb) : #On parcourt les différentes monojonctions
                self.Pabs = self.Pabs + self.monojunction_tab[index].Pabs #On somme les puissances absorbées de chaque monojonctions
    
    ##Calcul de la puissance électrique##
    def set_Pelec(self) :
        for index in range(self.nb) : #On parcourt les différentes monojonctions
            self.Pelec = self.Pelec + self.monojunction_tab[index].Pmax #On somme les puissances électriques de chaque monojonctions
    
    ##Affichage des résultars simplifié##
    def printMe(self) :
        print("")
        print("Common parameters: T =", self.T, "K,", "spectrum =", self.spectrum)
        print("")
        
        for index in range(self.nb) : #On affiche les caractéristiques de chaque monjonctions
            print("Cell no°", index +1, ": Eg =", self.monojunction_tab[index].Eg, "eV,", "Efficiency =", round(self.monojunction_tab[index].eff, 2), "%")
        
        print("Total efficiency:", self.eff, "%") #On affiche le rendement total de la cellule
    
    ##Affichage complet des résultats##
    def printDetails(self) :
        print("")
        print("T =", self.T, "K,", "spectrum =", self.spectrum)
        
        for index in range(self.nb) : #On affiche les caractéristiques de chaque monjonctions
            
            print("")
            print("Cell no°", index +1, "parameters:")
            print("")
            print("Eg =", self.monojunction_tab[index].Eg, "eV,", "Emax =", round(self.monojunction_tab[index].Emax, 2), "eV,")
            print("Calculated values:")
            print("jsc =", round(self.monojunction_tab[index].jsc, 2), "mA.cm-2")
            print("Voc =", round(self.monojunction_tab[index].Voc*1000, 2), "mV")
            print("Vmpp =", round(self.monojunction_tab[index].Vmpp*1000,2), "mV")
            print("jmpp =", round(self.monojunction_tab[index].jmpp, 2), "mA.cm-2")
            print("Pmax =", round(self.monojunction_tab[index].Pmax, 2), "W.m-2")
            print("Efficiency =", round(self.monojunction_tab[index].eff, 2), "%")
            print("Pin = ", round(self.monojunction_tab[index].Pin, 2), "W.m-2")

        print("Total efficiency:", self.eff, "%") #On affiche le rendement total de la cellule
