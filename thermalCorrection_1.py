###################################################
##Import des différentes bibliothèques à utiliser##
###################################################

from multijunction import Multijunction
import numpy as np
import csv
from scipy import constants, integrate, interpolate
import math

#############################
##Définition des constantes##
#############################

stefan = constants.Stefan_Boltzmann # Stefan-Boltzmann (W/m2/K4)
h_si = constants.Planck # Planck (J.s)
c = constants.c # Vitesse de la lumière (m/s)     
k = constants.Boltzmann # Boltzmann (J/K)
q = constants.value('elementary charge') #Charge élèmentaire (C)
pi = constants.pi #Pi

#######################################################################
##Calcul de la puissance émise par la cellule grâce à la luminescence##
#######################################################################

def Pem_calc(MJ, T):
    
    ##Initialisation##    
    Pem = 0.0 #Puissance émise
    C1 = 2.0 * constants.pi / (h_si**3 * c**2) #Constante de calcul C1
    C2 = k * T #Constante de calcul C2
    
    ##Calcul de la puissance##
    for index, value in enumerate(MJ.Eg_tab) : #On parcours toutes les multijonctions
        
        V = MJ.monojunction_tab[index].Vmpp #Récupération de la tension au point de puissance maximum
        bb = lambda E : C1 * E**3 * 1 / ( math.exp(( E - q * V) / C2) - 1 ) #Calcul du spectre émis
        BB, err = integrate.quad(bb, MJ.monojunction_tab[index].Eg * q, MJ.monojunction_tab[index].Emax * q, limit = 100)#Intégration du spectre émis pour avoir la puissance BB
        Pem = Pem + BB #Ajout de la puissance émise par la jonctions étudiée aux autres jonctions de la cellule.

    return Pem

###############################################################################
##Calcul de la puissance thermique émise par l'atmosphère à partir de données##
###############################################################################

def Pin_th_calc(Gap, MJ, em):
    
    ##Initialisation##
    wl_tab = [] #Tableau contenant les longueures d'ondes
    irr_tab = [] #Tableau contenant la puissance
    
    
    ##Lecture du fichier contenant les longeures d'ondes et les puissances associées##
    with open("dat/T_288.15K-USstd-4-100.csv", "r") as csv_file: #Ouverture du fichier
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        #Remplissage des tableaux de longeures d'ondes et de puissance à partir des données du fichier
        for lines in csv_reader :
            wl_tab.append(float(lines[0]))
            irr_tab.append(float(lines[1]))
    
    ##Calcul de la puissance##        
    f = interpolate.interp1d(wl_tab, irr_tab) #Interpolation des données
    
    lambda_min = min(wl_tab) #Récupération de lambda_min
    lambda_max = max(wl_tab) #Récupération de lambda_max

    def produit(wl): #Prise en compte de l'émissivité de la cellule
        return f(wl)*emissivity(wl, em)

    Pin_th, err = integrate.quad(produit, lambda_min, lambda_max, limit = 100) #Intégration des données pour avoir la puissance  

    return Pin_th


#########################################################
##Calcul de la puissance thermique émise par la cellule##
#########################################################

def Pem_th_calc(Eg, T, em):
    
    ##Initialisation##
    C1 = 2.0 * constants.pi / (h_si**3 * c**2) #Définition de la constante d'intégration C1
    C2 = k * T #Définition de la constante d'intégration C2
    E_max = 1.240 * q / 4 #Définition d'une borne supérieur pour l'intégration (au dessus de cette énergie la puissance émise est quasi nul quel que soit la température)
    
    ##Calcul de la puissance##
    bb = lambda E : C1 * E**3 * 1 / ( math.exp(E / C2) - 1 ) #Définition du spectre d'émission thermique de la cellule

    def produit(E): #Prise en compte de l'émissivité de la cellule
        return bb(E)*emissivity(1.240 * q/E,em)

    Pem_th, err = integrate.quad(produit, 0, 1.240*q/4, limit = 100) #Intégration du spectre d'émission thermique de la cellule pour avoir la puissance

    return Pem_th

###############################
##Paramètrage de l'émissivité##
###############################

def emissivity(wl, em):
    
    if em == "selective": #Choix de l'émissivité selective
        
        if wl < 13 and wl > 8 : #Émissivité de 8 à 13 micro-mètre
            return 1
        else :
            return 0
        
    elif em == "broadband": #Choix de l'émissivité large spectre
        
        if wl > 4 : #Borne inférieur du spectre à partir de laquelle l'émissivité de la cellule est quasi-nulle (elle n'est pas à 0 pour éviter un problème de divergence dans le calcul de l'énergie)
            return 1
        else :
            return 0

############################
##Calcul de la température##
############################

def thermal_correction(MJ, Ta, h, th_irradiance, em, Print, Delta, precision, dV, irradiation) :
    
    ##Initialisation##
    Temp = 0 #Initialisation d'une température intérmédiaire de calcul
    Gap = MJ.Eg_tab #Récupération des gaps de la cellule en eV
    s = MJ.spectrum #Récupération du type de spectre utilisé
    Tc = Ta+Delta #Définition de la température de la cellule comme étant la température ambiante + un delta initial défini par l'utilisateur
    th_irr = th_irradiance #Type de calcul pour le spectre d'émission thermique de l'atmosphère
    i = 0 #Définition d'un indice utilisé pour suivre le nombre d'itération réalisé par la boucle
    
    ##Calcul de la température##
    while abs(Temp - Tc) > precision : #Boucle permettant de continuer tant que ça n'a pas atteint la précision demandé
            
        ##Initialisation des différents termes du bilan thermique et de la multijoncton##
        MJ = Multijunction(Eg_tab = Gap, T = Tc, spectrum = s, dV = dV, irradiation = irradiation) #Définition d'une multijonction avec une température Tc
        Temp = Tc #On remplace la température intermédiaire de calcule par Tc
        Psun = MJ.Pabs #On récupère la puissance émise par le soleil
        Pem =  Pem_calc(MJ, Tc) #On calcul de la puissance émise par la cellule grâce à la luminescence
        Pelec = MJ.Pelec #On récupère  la puissance électrique de la cellule
            
        if th_irr == "boltzmann" : #Dans le cas d'un calcul suivant le modèle de boltzman
            
            #Résolution de l'équation de bilan thermique
            coeff = [stefan, 0, 0, h, Pem + Pelec - h * Ta - stefan * Ta**4 - Psun]
            sol = np.roots(coeff)
       
        if th_irr == "data": #Dans le cas d'un calcul suivant des données pour l'émission thermique de l'atmosphère
            
            #Ajout des puissances thermiques liées aux données
            Pin_th = Pin_th_calc(Gap, MJ, em) #Calcul de la puissance thermique émise par l'atmosphère
            Pem_th = Pem_th_calc(Gap, Tc, em) #Calcul de la puissance thermique émise par l'atmosphère
            
            #Résolution de l'équation de bilan thermique
            coeff = [h, Pem + Pelec - h * Ta  - Psun - Pin_th + Pem_th]
            sol = np.roots(coeff)

        ##Récuparation du résultat##
        for index, value in enumerate(sol) : #Récupération de la solution réel et positive l'équation du bilan thermique précédent
            if sol[index].imag == 0 :
                if sol[index] > 0 :
                    Tc = sol[index].real

        if Tc == Temp : #Vérifie qu'une solution a bien été trouvé
            print("No solution found")
        
        Tc = Temp + 0.5 * (Tc - Temp) #Calcul de la nouvelle température (située entre l'ancienne et la nouvelle)   

        ##Suivi de l'avancé de la boucle##
        i=i+1
        print("Itération : " + str(i)) #Pour voir le nombre d'itération
        print("T_cal = ",Tc," K") #Pour voir si il y a un problème de converegence

    ##Calcul de la cellule finale avec la température d'équilibre##
    MJ = Multijunction(Eg_tab = Gap, T = Tc, spectrum = s, dV=dV, irradiation = irradiation)
                
    #Visualisation textuelle des résultats##
    if Print == "true" :
        print("Tc =", Tc, "K")
        print("eff =", MJ.eff, "%")
        print("")
        print("Pem =", Pem, "W/m2")        
        print("Psun =", Psun, "W/m2")
        print("Pelec =", Pelec, "W/m2")
        print("Pin_th =", Pin_th, "W/m2")
        print("Pem_th =", Pem_th, "W/m2")
        print("Pcon =", h * (Ta - Teq), "W/m2")
    eff = MJ.eff         
    return MJ

