###################################################
##Import des différentes bibliothèques à utiliser##
###################################################

from Smarts import spectre as spectre
import Heure
Heure.sunriseClass()
from timezonefinder import TimezoneFinder
import os
import json
import pytz
import csv
import numpy as np
from multijunction import Multijunction
from monojunction_1 import Monojunction
from thermalCorrection_1 import thermal_correction, Pem_th_calc
from pylab import *
from datetime import datetime

#############
##Préambule##
#############

#Implémentation des différentes fonction permettant de traiter les problèmes d'heure
mySunrise=Heure.sunriseClass() # instance mySunrise appartenenat à la classe Heure, elle sert pour le calcul du lever et du coucher du soleil
tf = TimezoneFinder() # objet tf appartenant au module TimezoneFinder. tf sert au calcul de l'offset avec l'UTC

#Traitement des fichiers TMY (en .json)
Liste_pays=os.listdir('Base_tmy2/') #Permet de lister les localisation pour lesquelles les fichier TMY sont dans le dossier 'Base_tmy2'
if '.DS_Store' in Liste_pays :
    del Liste_pays[Liste_pays.index('.DS_Store')] #Supprime, si il existe le fichier '.DS_Store'

#Définition des paramètres
spectrum = "BB" # Définition du spectre "AM0G", "AM1.5G", "AM1.5D", "BB" ou "SMARTS" pour utiliser le spectre généré par le code SMARTS
em = "selective" #Choix de l'émissivité : "broadband" ou "selective"
h = 2 #Définition du coefficient de transfert de chaleur par convexion en W.m-2.K-1
th_irradiance = "data" #Choix de l'irradiance de l'atmosphère "boltzmann" ou "data"
Gap = [2.28,1.69,1.30,0.97,0.66] #On défini les gaps de la cellule multijonction en eV
DeltaTinit = 30 #Défini un offset de base entre la température ambiante et la température de la cellule.
time_start = 1 #Définition de l'heure de l'année à partir de laquelle on réalise la simulation (min = 1; max = 8760)
time_stop = 24 #Définition de l'heure de l'année à partir de laquelle la simulation s'arrête (min = 1; max = 8760)
precision = 0.1 #Définition de la précision demandé sur le calcul de la température +- precision
dV = 0.001 #Définition du pas de tension pour le calcul des caractéristiques éléctriques

#Vérification des paramètres
if(time_start>time_stop):
    print("ERREUR : time_start est superieur à time_stop")
    sys.exit()

################################
##Réalisation de la simulation##
################################

for C in Liste_pays: #Boucle pour réaliser la simulation pour tous les localisations présentes dans le dossier 'Base_tmy2/'
    
    ## Préambule à la simulation pour une localisation ##

    # Importation du fichier TMY correspondant aux localisations:
    with open('Base_tmy2/'+C) as json_data:
              data_dict = json.load(json_data) # On récupère toutes les donnée JSON
    TMY=data_dict.get('outputs').get('tmy_hourly') # On récupère les données heure par heure dans le dictionnaire TMY
    Long=data_dict.get('inputs').get('location').get('longitude') # On récupère la longitude (Long) de la localisation étudiée
    Lat=data_dict.get('inputs').get('location').get('latitude') # On récupère la latitude (Lat) de la localisation étudiée
    Elev=data_dict.get('inputs').get('location').get('elevation') # On récupère l'altitude (Elev) de la localisation étudiées
    
    #Initialisation des données a récupérer heure par heure
    Rendement=np.array([]) #Définition d'un tableau vide qui va contenir le rendement pour chaque heure
    Temperature=np.array([]) #Définition d'un tableau vide qui va contenir la température de la cellule pour chaque 
    Temperature_ambiante = [] #Définition d'un tableau vide qui va contenir la température ambiante pour chaque heure

		#Initialisation des données
    DT = DeltaTinit #Réintialisation de l'offset de base pour chaque localisations
		
    #calcul de l'offset avec UTC (décallage entre l'heure UTC et l'heure locale)
    UTC_offset=int((datetime.now(pytz.timezone(tf.timezone_at(lng=Long, lat=Lat)  )).strftime('%z'))[0:3])	
    
    #Création d'un fichier csv pour conserver les résultats par localisations
    with open('data_calc_'+C.removesuffix('.json')+'_'+em+'.csv','w',newline='') as sd :
		    crsd=csv.writer(sd)
		    crsd.writerow(['H','R','Tcell','Tambiante']) #Créer une colonne pour l'heure, le rendement, la température de la cellule, la température ambiante
    
    ## Simulation heure par heure pour la localisation choisi ##

    for z in range(time_stop-time_start+1) : #boucle permettant de parcourir les différentes heures étudiées, z = nombre d'heure traité        
        
        #Définition des paramètres
        H = z + time_start -1 #Définition de l'heure par rapport à l'heure de départ et au nombre d'heure traité
        #Récupération des données TMY
        Temperature_ambiante.append(TMY[H].get('T2m')+273.15) # on récupére la valeur de la température ambiante en K dans le dictionnaire TMY
        date=datetime.strptime((TMY[H].get('time(UTC)')),'%Y%m%d:%H00') # on récupére la date et l'heure UTC dans le dictionnaire TMY
        
        #Calcul de l'heure de levé et de couché du soleil
        mySunrise.setNumericalDate(date.day,date.month,date.year) # on donne la date
        mySunrise.setLocation(Lat,Long) # on donne la location
        mySunrise.calculateWithUTC(UTC_offset) # on donne le décallage entre l'heure UTC et l'heure locale
        h1=np.ceil(mySunrise.sunriseTime) # calcul de l'heure de levé du soleil pour la date donnée (arrondi à l'entier supérieur par précaustion à cause de smarts)
        h2=np.floor(mySunrise.sunsetTime) # calcul de l'heure de couché du soleil pour la date donnée (arrondi à l'entier inférieur par précaustion à cause de smarts)
        heure=date.hour+UTC_offset #convertion de l'heure UTC vers heure locale (car smarts utilise l'heure local). Attention: smarts ne prend pas en compte le changement d'heure en été!
        
        cSpectre_v = 1 #Variable permettant d'indiquer que le calcul du spectre est valide

        #Calcul du spectre si nécessaire
        if spectrum == "SMARTS":
            Spectre=spectre([date.year,date.month,date.day,heure,Lat,Long,UTC_offset],[Lat,Elev*10**(-3),0]) # on fait appelle à Smarts pour générer le spectre (fonction "spectre" du script "Smarts")
            if isinstance(Spectre, int) == False and sum(Spectre[:,1]) != 0 : #Verification du calcul du spectre
                cSpectre_v = 1
            else :
                cSpectre_v = 0

        if h1<=heure<=h2 and cSpectre_v == 1 and TMY[H].get('Gb(n)')!=0: #Condition pour vérifier que le soleil est bien levé
            #Calcul des paramètres de la cellule
            Panel = Multijunction(Eg_tab = Gap, T = Temperature_ambiante[z], spectrum = spectrum,dV=dV, irradiation = TMY[H].get('Gb(n)')) #Définition d'une cellule multijonction
            NewPanel = thermal_correction(MJ = Panel, Ta = Temperature_ambiante[z], h = h, th_irradiance = th_irradiance, em = em, Print = "false",Delta=DT, precision = precision, dV = dV, irradiation = TMY[H].get('Gb(n)')) #Application du modèle thermique à la multijonction défini précédémment

            #Récupération des données de la cellule
            Rendement=np.append(Rendement,NewPanel.eff) #Récupération du rendement de la cellule (en %)
            Temperature=np.append(Temperature,NewPanel.T-273.15) # Récupération de la température de la cellule (en °C)
            
            #Enregistrement des données dans le fichier csv
            with open('data_calc_'+C.removesuffix('.json')+'_'+em+'.csv','a',newline='') as sd :
			          crsd=csv.writer(sd)
			          crsd.writerow([H,NewPanel.eff,NewPanel.T-273.15,Temperature_ambiante[z]-273.15])
            
            #Préparation de la prochaine étape de calcul
            DT=NewPanel.T-Temperature_ambiante[z] #On essaye de prédire la différence entre la température de la cellule et la température de la cellule. Ça permet d'optimiser le calcul

            #Contrôle de la bonne avancé du programme
            print("Heure :")
            print(H)
    
    ## Tracé des résultats pour une localisation ##

    t = np.arange(1,np.size(Rendement)+1, 1)

    fig1, ax = plt.subplots()
    ax.scatter(t, Rendement, label='Emissivite selective')
    ax.set(xlabel='Heure', ylabel='Rendement (%)')
    ax.legend()
    ax.grid()
    ax.set_ylim(40, 80)
    ax.set_xlim(0, np.size(Rendement)+1)
    plt.show()

    fig2, ax = plt.subplots()
    ax.scatter(t, Temperature, label='Emissivite selective')
    ax.set(xlabel='Heure', ylabel='Température (°C)')
    ax.legend()
    ax.grid()
    ax.set_ylim(20, 80)
    ax.set_xlim(0, np.size(Rendement)+1)
    plt.show()            
