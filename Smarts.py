#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Auteur: Jean-Baptiste Barbez


Génération du spectre du soleil

Ce script est une fonction (spectre) qui génère le fichier .txt contenant les données d'entrée 
pour l'utilisation du code SMARTS. La fonction lance ensuite l'exécution des calculs de SMARTS.

Dependances : Dossier 'Gases'| Dossier 'CIE_data' | Dossier 'Albedo'| Dossier 'spectres_files'| Dossier 'Solar'| Fichier 'smarts295.inp.txt'

              Fichier 'smarts295.f'| Fichier 'smarts295.exe' |

"""

###################################################
##Import des différentes bibliothèques à utiliser##
###################################################

import os
import shutil
import csv
import numpy as np
import matplotlib.pyplot as plt

####################################################################################################################################
##Définition de la fonction spectre permettant de créer le fichier de configuration utiliser par le code SMARTS (écrit en FORTRAN)##
####################################################################################################################################

def spectre(Card17a,Card2a):
    
         

     Card1='SIMULATION' #titre
     Card2=2 # 0 pour fixer la pression(hPa)
             # 1 pour pression altitude(km) et hauteur(élévation par rapport à la terre) (km)
             # 2 latitude altitude et hauteur

     Card3=1   # 0 pour fixer la T° athmosphérique (°C), l'humidité relative (#), la saison (winter ou summer), T° moyenne de la journée
               # 1 pour sélectionner une des dix référence d'athmosphere 
     Card3a='USSA'

     Card4=1 # 0 pous fixer sois-même la valeur de la vapeur d'eau dans Card4a (g.cm-2)
             # 1 pour qu'elle soit fixer par la référence d'athmosphere choisie à Card3 et par l'altitude( si Card3 =! 1, c'est USSA) 
             # 2 pour qu'elle soit calculée à partir de la T° moyenne de la journée et de l'humidité relative (non recommé car approximatif, en particulier si hauteur>0) 

     # présent que si Card4=0 :

     Card4a=7           

         
         
     Card5=1   # 0 pour choisir la correction appropriée de l’altitude de la couche d’ozone et le niveau de la mer ou le niveau du site
               # 1 pour que niveau de la mer ou le niveau du site soit déterminé par la référence d'athmosphere choisie à Card3 ( si Card3 =! 1, c'est USSA)

     # présent que si Card5=0 :

     Card5a=[0,0]                 # Correction appropriée de l’altitude de la couche d’ozone 
                                             # 0 pour contourner la correction d’altitude de la couche d'ozone (à partir du niveau du site)
                                             # 1 pour appliquer une correction de l'abondance de la couche d'ozone (à partir de données au niveau de la mer) dans le cas d'un site élevé ou avec une hauteur
                                    
                                  # Niveau du site (atm-cm) si Correction=0, niveau de la mer (atm-cm) si Correction = 1
                                                                          
    
         # Card6 pour définir les bonnes conditions de l'absorption des gaz et de la pollution atmosphérique                                                 

     Card6=1  # 0 en prenant en compte les gaz supplémentaires (correspondant aux gaz dans la troposphère due à la pollution)
              # 1 on utilise des valeurs par défauts pour les gaz supplémentaires (à part le CO2,l'ozone et la vapeur d'eau calculés avec Card4a, 5a et 7)  

     # présent que si Card6=0 :

     Card6a=0  # 0 si on considère 10 polluants
         # 1 pour sélectionner des conditions atmosphériques par défaut
         # + que 10 polluants:
         # 2 pollution légère 
         # 3 pollution modérée
         # 4 pollution sévère
     Card6b=0
     Card7=370
     Card7a=0
     Card8='S&F_RURAL'
     Card8a=0
     Card9=0
     Card9a=0.084
     Card10=0
     Card10a=0
     Card10b=1 
     Card10c=[38, 30, 180]
     Card10d=0
     Card11= [280, 4000, 1.0, 1367.0]
     Card12=2
     Card12a=[280, 4000, 0.5]
     Card12b=1  
     Card12c=6 #Direct normal irradiance
     Card13=0
     Card13a=[0, 2.9, 0]
     Card14=0
     Card14a=0
     Card15=0
     Card16=0
     Card17=3
     #Card17a=[2015,6,1,18,45.759,4.83,1]           
    
    
       # Modification du fichier .txt selon les valeurs choisie pour les Cards
       
     fid=open('smarts295.inp.txt','w')
     fid.write(str(Card1)+'   !Card1')
     fid.write("\n"+str(Card2)+'   !Card2')
     fid.write("\n"+str(Card2a[0])+' '+str(Card2a[1])+' '+str(Card2a[2])+'   !Card2a')
     fid.write("\n"+str(Card3)+'   !Card3')

     if Card3==1:
         fid.write("\n"+"'"+Card3a+"'"+'   !Card3a')
     else:
         fid.write("\n"+str(Card3)+'   !Card3')
   
     fid.write("\n"+str(Card4)+'   !Card4')
 
     if Card4==0:
     
       fid.write("\n"+str(Card4a)+'   !Card4a')
 
     fid.write("\n"+str(Card5)+'   !Card5')
 
     if Card5==0:
         fid.write("\n"+str(Card5a[0])+' '+str(Card5a[1])+   '!Card5a')
  
     fid.write("\n"+str(Card6)+'   !Card6')

     if Card6==0:
         fid.write("\n"+str(Card6a)+'   !Card6a')
         if Card6a==0:
             fid.write("\n"+str(Card6b)+'   !Card6b')
         
     fid.write("\n"+str(Card7)+'   !Card7')
     fid.write("\n"+str(Card7a)+'   !Card7a')
     fid.write("\n"+"'"+Card8+"'"+'   !Card8')

     if Card8=='USER':
         fid.write("\n"+str(Card8a)+'   !Card8a')
 
     fid.write("\n"+str(Card9)+'   !Card9')
     
     if Card9==0:
         fid.write("\n"+str(Card9a)+'   !Card9a')      
  
     fid.write("\n"+str(Card10)+'   !Card10')
 
     if Card10==-1:
         fid.write("\n"+str(Card10a)+'   !Card10a')      
  
     fid.write("\n"+str(Card10b)+'   !Card10b')
 
     if Card10b==1:
         fid.write("\n"+str(Card10c[0])+' '+str(Card10c[1])+' '+str(Card10c[2])+'   !Card10c')
         if Card10c[1]==-1:
             fid.write("\n"+str(Card10d)+'   !Card10d')
       
 
 

     fid.write("\n"+str(Card11[0])+' '+str(Card11[1])+' '+str(Card11[2])+' '+str(Card11[3])+'   !Card11')
     fid.write("\n"+str(Card12)+'   !Card12')
     
     if Card12>=1:
         fid.write("\n"+str(Card12a[0])+' '+str(Card12a[1])+' '+str(Card12a[2])+'   !Card12a')      
 
     if (Card12==2) or (Card12==3):
         fid.write("\n"+str(Card12b)+'   !Card12b')
         fid.write("\n"+str(Card12c)+'   !Card12c')  
 
     fid.write("\n"+str(Card13)+'   !Card13')
     
     if Card13==1:
         fid.write("\n"+str(Card13a[0])+' '+str(Card13a[1])+' '+str(Card13a[2])+'   !Card13a')      
 
     fid.write("\n"+str(Card14)+'   !Card14')

     if Card14==1:
         fid.write("\n"+str(Card14a)+'   !Card14a')      
 
     fid.write("\n"+str(Card15)+'   !Card15')
     fid.write("\n"+str(Card16)+'   !Card16')
     fid.write("\n"+str(Card17)+'   !Card17')
     fid.write("\n"+str(Card17a[0])+' '+str(Card17a[1])+' '+str(Card17a[2])+' '+str(Card17a[3])+' '+str(Card17a[4])+' '+str(Card17a[5])+' '+str(Card17a[6])+'   !Card17a')

     fid.close()  # fermeture du fichier
    
    
     if os.path.isfile('spectres_files/smarts295.ext.csv')==True:
          os.remove('spectres_files/smarts295.ext.csv') #supression de l'ancien fichier .ext.txt
     if os.path.isfile('spectres_files/smarts295.out.csv')==True:     
          os.remove('spectres_files/smarts295.out.csv') #supression de l'ancien fichier .out.txt
     os.system('./smarts295') #lancement du code SMARTS
     os.rename('smarts295.ext.txt','smarts295.ext.csv')
     os.rename('smarts295.out.txt','smarts295.out.csv')
     shutil.move('smarts295.ext.csv', 'spectres_files')#ajout du nouveau fichier .ext.txt
     shutil.move('smarts295.out.csv', 'spectres_files')#ajout du nouveau fichier .out.txt

     #Verification que SMARTS génère bien un spectre
     if os.stat('./spectres_files/smarts295.ext.csv').st_size >1000 :
          data = np.genfromtxt('./spectres_files/smarts295.ext.csv',skip_header=1) # enregistrement des données spectrales dans data
          return data
     else :
          return 1
     

     
  
     
