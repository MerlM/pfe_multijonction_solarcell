# -*- coding: utf-8 -*-
"""last edited : 09/03/2021
Class contains:
Object Monojunction and related functions
--> example of an object creation : Cell = Monojunction(Eg = 3, Emax = 10,T = 298, spectrum = "AM1.5G")
energies in eV, T in K, spectrum choices : "AM1.5D", "AM1.5G", "AM0G", "BB"

Functions:
--> Cell.printMe() to print the cell parameters (Eg, Emax, Tc, spectrum) 
and the calculated values (jsc, Voc, Vmpp, jmpp, Pmax, Efficiency and Pin)
"""
###################################################
##Import des différentes bibliothèques à utiliser##
###################################################

import math
import csv
import numpy as np
from scipy import constants, integrate, interpolate


#############################
##Définition des constantes##
#############################

pi = constants.pi
q = constants.value('elementary charge') # Elementary charge (C)
h_si = constants.Planck # Planck constant (J.s)
h = h_si / q # Planck constant (eV.s)
c = constants.c  # Speed of light (m/s)
k_si = constants.Boltzmann # Boltzmann constant (J/K)
k = k_si / q # Boltzmann constant (eV/K)
stefan = constants.Stefan_Boltzmann # Stefan-Boltzmann constant (W/m2/K4)
    
Tsun = 6000.0 # Sun's apparent temperature (K)
omega_sun = 6.85E-5 #solid angle sustained by the sun

########################################
##Définition d'une class multijonction##
########################################

class Monojunction :

    ##Constructeur de la class##
    def __init__(self, Eg, Emax, T, spectrum, dV, irradiation):
        
        # Attributs

        self.Eg = Eg #Gap du semi-conducteur (eV)
        self.Emax = Emax #Energie max du spectre (eV)
        self.T = T #Température de la cellule (K)
        self.spectrum = spectrum #Définition du spectre solaire
        self.dV = dV #Définition du pas de tension pour le calcul des caractéristiques éléctriques (V)
        self.Pin = .0 #Puissance incidente totale (W/m2)
        self.Pabs = .0 #Puissance absorbée (W/m2)
        self.nin = .0 #Nombre de photon absorbé <=> nombre d'électron généré
        self.Pmax = .0 #Puissance maximum (W/m2)
        self.jmpp = .0 #Courant au point de puissance maximum (mA/cm2)
        self.Vmpp = .0 #Tension au point de puissance maximum (V)
        self.Voc = .0 #Tension de circuit ouvert (V)
        self.jsc = .0 #Courant de court circuit (mA/cm2)
        self.eff = .0 #Rendement
        self.irradiation = irradiation #Irradiation direct normal mesurée
        self.factor = .0 #Ratio entre puissance incidente mesurée/puissance incidente calclul        

        #Définition de tableau pour les calculs
        self.V_tab = [] #Tableau contenant les tensions
        self.n_tab = [] # Tableau contenant le nombre de photon reçu
        self.nem_tab = [] #Tableau contenant le nombre de photon émis
        self.j_tab = [] #Tableau contenant les courants
        self.wl_tab = [] #Tableau contenant les longueurs d'ondes (nm)
        self.P_tab = [] #Tableau conteant les puissances électriques
        self.Pin_tab = [] #Tableau contenant les puissances incidentes
        
        ##Définition des fonctions##
        self.incident() #Calcul de : la puissance incidente, la puissance absorbé et le nombre de photon absorbé
        self.j_P_calculations() #Calcul des caractéristique électriques
        self.set_Pmax()	#Calcul de Pmax
        self.set_eff() #Détermination du rendement		 
            
    ##Calcul de : la puissance incidente, la puissance absorbé et le nombre de photon absorbé##
    def incident (self):
        
        if self.spectrum == "BB" : #Calcul pour un spectre solaire modélisé par le corps noir
            
            f = lambda x: x**2 / (math.exp(x) - 1) #Fonction du corps noir avec un changement de variable x = E/(k*T_sun)
            F, err = integrate.quad(f, self.Eg / (k * Tsun), self.Emax / (k * Tsun)) #Intégration de la fonction du corps noir
            self.Pin = omega_sun / pi * stefan * Tsun**4 #Calcul de la puissance incidente
            self.factor = self.irradiation/self.Pin #Détermination du facteur entre condition réel et théorique
            self.Pin = self.Pin*self.factor #Calcul de la puissance incidente
            self.nin = self.factor*F * omega_sun * Tsun**3 * 2 * k**3 / (h**3 * c**2) #Calcul du nombre de photon incident
    
            #Calcul de la puissance absorbé
            C1 = 2.0 * h_si * c**2 #Constante de calcul C1
            C2 = h_si * c / k_si #Constante de calcul C2
            bb = lambda x: omega_sun * C1 * x**(-5) * 1 / ( math.exp(C2/(x*Tsun)) - 1 ) #Calcul de la fonction représentant le spectre incidente
            
            lambda_max = h_si * c / (self.Eg * q)  #Calcul du lambda_max absorbé
            lambda_min = h_si * c / (self.Emax * q) #Calcul du lambda_min absorbé
            irradiance, err = integrate.quad(bb, lambda_min, lambda_max, epsrel = 1E-3, limit = 50)  #Intégration du spectre incident pour les longueurs d'ondes absorbées
            self.Pabs = irradiance #Récupération de la puissance absorbé

        elif self.spectrum == "AM1.5G" or self.spectrum == "AM0G" or self.spectrum == "AM1.5D" or self.spectrum == "SMARTS": #Calcul pour des spectres autres que le corps noir
            
            ##Lecture des données##
            if self.spectrum == "AM1.5G":
                
                with open("dat/ASTMG173.csv", "r") as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    
                    for lines in csv_reader : #Parcourt chaque longueur d'onde

                        wl = float(lines[0]) #Récupération de la longueur d'onde
                        self.wl_tab.append(wl) #Stockage de la longueur d'onde
                        self.n_tab.append(float(lines[2]) / (1240.0 * q / wl)) #Récupération du nombre de photon
                        self.Pin_tab.append(float(lines[2])) #Récupération de la puissance incidente

            elif self.spectrum == "AM0G" :

                with open("dat/ASTMG173.csv", "r") as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    
                    for lines in csv_reader: #Parcourt chaque longueur d'onde

                        wl = float(lines[0])  #Récupération de la longueur d'onde
                        self.wl_tab.append(wl) #Stockage de la longueur d'onde
                        self.n_tab.append(float(lines[1]) / (1240.0 * q / wl)) #Récupération du nombre de photon
                        self.Pin_tab.append(float(lines[1])) #Récupération de la puissance incidente

            elif self.spectrum == "AM1.5D" :
                
                with open("dat/ASTMG173.csv", "r") as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    
                    for lines in csv_reader : #Parcourt chaque longueur d'onde

                        wl = float(lines[0]) #Récupération de la longueur d'onde
                        self.wl_tab.append(wl) #Stockage de la longueur d'onde
                        self.n_tab.append(float(lines[3]) / (1240.0 * q / wl)) #Récupération du nombre de photon
                        self.Pin_tab.append(float(lines[3])) #Récupération de la puissance incidente

            elif self.spectrum == "SMARTS" :

                with open("spectres_files/smarts295.ext.csv", "r") as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=' ')
                    next(csv_reader) #Pour sauter l'entête du fichier .csv
                    
                    for lines in csv_reader : #Parcourt chaque longueur d'onde

                        wl = float(lines[0]) #Récupération de la longueur d'onde
                        self.wl_tab.append(wl) #Stockage de la longueur d'onde
                        self.n_tab.append(float(lines[1]) / (1240.0 * q / wl)) #Récupération du nombre de photon
                        self.Pin_tab.append(float(lines[1])) #Récupération de la puissance incidente
            
            #Calcul Pin  
            f = interpolate.interp1d(self.wl_tab, self.Pin_tab) #Interpolation des données récupérées pour avoir un spectre
            
            lambda_min = min(self.wl_tab) #Détermination du lambda_min des données récupérées
            lambda_max = max(self.wl_tab) #Détermination du lambda_max des données récupérées

            I, err = integrate.quad(f, lambda_min, lambda_max, limit = 50) #Intégration du spectre pour avoir une puissance
            
            self.Pin = I # Récupération de la puissance incidente
            self.factor = self.irradiation/self.Pin #Détermination du facteur entre condition réel et théorique
            self.Pin = self.Pin*self.factor #Application du facteur entre condition réel et théorique
            
            #Calcul des bornes d'absorbtion
            lambda2_min=max(min(self.wl_tab),1240 / self.Emax) #Calcul du lambda_min absorbé
            lambda2_max=min(max(self.wl_tab),1240 / self.Eg) #Calcul du lambda_max absorbé

            #Calcul Nabs
            g = interpolate.interp1d(self.wl_tab, self.n_tab) #Interpolation du nombre de photon en focntion de la longueur d'onde
            
            G, err2 = integrate.quad(g, lambda2_min, lambda2_max, limit = 50) #Intégration du nombre de photon en fonction de la longueur d'onde pour avoir le nombre de photon total
            self.nin = G*self.factor #Application du facteur entre condition réel et théorique; stockage du nombre de photon absorbé
            
            #Calcul Pabs
            F2, err = integrate.quad(f, lambda2_min, lambda2_max, limit = 50) #Intégration du spectre pour les longueurs d'ondes absorbées pour avoir la puissance absorbée
            self.Pabs = F2*self.factor #Stockage + application du ration entre condition réel et théorique    
        
    ##Calcul des caractéristique électriques##
    def j_P_calculations (self) :
        
        n = 0.0 #Initialisation du nombre de photon émis
        j = 0.1 #Courant initial pour le calcul
        V = 0 #Tension initial pour le calcul

        while j > 0 : #Calcul jusqu'au courant de circuit ouvert
            
            C = 2 * pi / (c**2 * h**3) #Constante pour le calcul

            #Nombre de photon émis
            nem = lambda E: C * E**2 / (math.exp((E - V) / (k * self.T)) - 1) #Calcul de la fonction régissant le nombre de photon émis en fonction de l'énergie
            n, err = integrate.quad(nem, self.Eg, self.Emax, limit = 50) #Intégration du nombre de photon émis dans le domaine des énergies absorbées
               
            F = self.nin - n #Bilan du nombre de photon
            j = F * q / 10  # 1000 / 100**2 # Calcul du courant en mA/cm²
            self.j_tab.append(j) #Stockage du courant
            self.P_tab.append(10 * j * V) # Calcul de la puissance électrique en W/m2
            self.V_tab.append(V) #Stockage de la tension
            V = V + self.dV #Incrémentation de la tension pour effectuer le balayage en tension
            
            
        
        self.Voc = V - 3*self.dV / 2 #Récupération de la tension de circuit ouvert qui correspond à la fin de la boucle (V)
        self.jsc = self.j_tab[0] #Récupération du courant de court circuit
    ##Calcul de Pmax##    
    def set_Pmax(self) :

        self.position = self.P_tab.index(max(self.P_tab)) #Détermination du point où la puissance est maximum
        self.Pmax=self.P_tab[self.position] #Récupération de la puisssance maximum
        self.jmpp=self.j_tab[self.position] #Récupération du courant au point de puissance maximum
        self.Vmpp=self.V_tab[self.position] #Récupération de la tension au point de puissance maximum
           
    ##Détermination du rendement##
    def set_eff(self):
        self.eff = self.Pmax / self.Pin * 100.0
        
        
    """
    ========================================
    Characteristic values
    ========================================
    """
    
    def printMe (self):
        print("")
        print("Cell parameters:")
        print("Eg =", self.Eg, "eV,", "Emax =", round(self.Emax, 2), "eV,", "T =", self.T, "K,", "spectrum =", self.spectrum)
        print("")
        print("Calculated values:")
        print("jsc =", round(self.jsc, 2), "mA.cm-2")
        print("Voc =", round(self.Voc*1000, 2), "mV")
        print("Vmpp =", round(self.Vmpp*1000,2), "mV")
        print("jmpp =", round(self.jmpp, 2), "mA.cm-2")
        print("Pmax =", round(self.Pmax, 2), "W.m-2")
        print("Efficiency =", round(self.eff, 2), "%")
        print("Pin = ", round(self.Pin, 2), "W.m-2")
