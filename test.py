# -*- coding: utf-8 -*-
"""
Last edited : 11/03/2021

--> Class main used to created object monojunction, multijunction and run thermal analysis to find the cell working temperature

Enter the cell parameters: Egap, Emax (max energy absorbed), temperature (K) and wanted sprectrum
as well as the thermal simulation parameters: h, th_irradiance, em

--> th_irradiance choices: "data", "boltzmann"
--> em choices: "broadband", "selectrive"
--> Spectrum choices: "AM0G", "AM1.5G", "AM1.5D", "BB"
--> Choose to plot power and current vs tension, only current or only power
--> Plot choices: "both", "power", "current"

--> Function printMe() prints the cell caracteristic values: 
jsc, Voc, Vmpp, jmpp, Pmax, Efficiency, Pin

"""

import matplotlib.pyplot as plt
import numpy as np
from multijunction import Multijunction
from monojunction_1 import Monojunction
from thermalCorrection_1 import thermal_correction, Pem_th_calc


"""
========================================
Choosing the simulation parameters
========================================
"""

# definition of the Panel
spectrum = "BB" # spectrum choices: "AM0G", "AM1.5G", "AM1.5D", "BB"
Gap = [1.4] #list of gaps in eV
Ta = 293.15 #ambiant temperature (K)
h = 10 #convection heat transfer coefficient (W.m-2.K-1)
th_irradiance = "data" #th_irradiance choices: "boltzmann", "data"
em = "selective" #em choices: emissivity profile "broadband" or "selective"



#to print the panel :
#Panel.printMe()
#Panel.printDetails()

"""
========================================
Creating a monojonction and printing it
========================================
"""
"""
Cell = Monojunction(Eg = 3, Emax = 10,T = 298, spectrum = "AM1.5G")
Cell.printMe()
"""

"""
========================================
Research of the multijunctions real working temperature
========================================
"""
Rendement_selective=np.array([])
Rendement_broadband=np.array([])
Temperature_selective=np.array([])
Temperature_broadband=np.array([])

em = "selective" 
Gap1 = [1.3] #list of gaps in eV
Gap2 = [1.68,0.96] #list of gaps in eV
Gap3 = [1.95,1.30,0.82] #list of gaps in eV
Gap4 = [2.12,1.51,1.08,0.70] #list of gaps in eV
Gap5 = [2.28,1.69,1.30,0.97,0.66] #list of gaps in eV
Gap6 = [2.40,1.82,1.45,1.15,0.88,0.60] #list of gaps in eV
Gap7 = [2.51,1.95,1.59,1.30,1.05,0.82,0.58] #list of gaps in eV
Gap8 = [2.59,2.04,1.68,1.41,1.17,0.96,0.75,0.52] #list of gaps in eV
Gap9 = [2.68,2.13,1.79,1.52,1.30,1.10,0.91,0.72,0.52] #list of gaps in eV
Gap10 = [2.73,2.20,1.86,1.60,1.38,1.19,1.01,0.84,0.67,0.48] #list of gaps in eV

Panel = Multijunction(Eg_tab = Gap1, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap2, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap3, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap4, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap5, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap6, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap7, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap8, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap9, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap10, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_selective=np.append(Rendement_selective,NewPanel.eff)
Temperature_selective=np.append(Temperature_selective,NewPanel.T-273.15)

em = "broadband" 

Panel = Multijunction(Eg_tab = Gap1, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap2, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap3, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap4, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap5, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap6, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap7, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap8, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap9, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)

Panel = Multijunction(Eg_tab = Gap10, T = Ta, spectrum = spectrum)
NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "true")
Rendement_broadband=np.append(Rendement_broadband,NewPanel.eff)
Temperature_broadband=np.append(Temperature_broadband,NewPanel.T-273.15)


print(Rendement_broadband)
print(Rendement_selective)

print(Temperature_broadband)
print(Temperature_selective)

t = np.arange(1, 11, 1)

fig1, ax = plt.subplots()
ax.scatter(t, Rendement_selective, label='Emissivite selective')
ax.scatter(t, Rendement_broadband, label='Emissivite broadband')
ax.set(xlabel='Nombre de jonction', ylabel='Rendement (%)')
ax.legend()
ax.grid()
ax.set_ylim(20, 65)
ax.set_xlim(0, 11)
plt.show()

fig2, ax = plt.subplots()
ax.scatter(t, Temperature_selective, label='Emissivite selective')
ax.scatter(t, Temperature_broadband, label='Emissivite broadband')
ax.set(xlabel='Nombre de jonction', ylabel='Température (°C)')
ax.legend()
ax.grid()
ax.set_ylim(40, 70)
ax.set_xlim(0, 11)
plt.show()

"""
========================================
Variation de la température ambinate
========================================
"""
"""
i = 0
eff_tab = [] #efficiency tab (%)
T_tab = [] #ambiant temperature tab (K)
Tc_tab = [] #cell temperature tab (K)


while Ta < 300 :
#Looking for the real cell temperature Tc
    NewPanel = thermal_correction(MJ = Panel, Ta = Ta, h = h, th_irradiance = th_irradiance, em = em, Print = "false")
    T_tab.append(Ta)
    Tc_tab.append(NewPanel.T)
    eff_tab.append(NewPanel.eff)
    Ta = Ta + 1
    i = i + 1
    print(i)

#prints mains values
print("")
print(T_tab)
#print("")
print("")
print(eff_tab)
print("")
print(Tc_tab)
print("")

#to display more details:
#NewPanel.printMe()
#Panel.printDetails()
"""


"""
========================================
Plots
========================================
"""
"""
# what to plot
plot = "both"   #possibles choices: "both", "power", "current"

cell_to_plot = Panel.monojunction_tab[0]

if plot == "current" :
    plt.plot(cell_to_plot.V_tab, cell_to_plot.j_tab, linewidth=1.5)
    plt.xlim(0, max(cell_to_plot.V_tab) + 0.05)
    plt.ylim(0, max(cell_to_plot.j_tab) + 5)
    plt.xlabel('Voltage (V)')
    plt.ylabel('Current (mA/cm2)')
    plt.savefig('res/current-voltage.png')

if plot == "power" :
    plt.plot(cell_to_plot.V_tab, cell_to_plot.P_tab, linewidth = 1.5)
    plt.xlim(0, max(cell_to_plot.V_tab) + 0.05)
    plt.ylim(0, max(cell_to_plot.P_tab) + 5)
    plt.xlabel('Voltage (V)')
    plt.ylabel('Power (W.m-2)')
    plt.savefig('res/power-voltage.png')

if plot == "both" :
    
    fig,ax = plt.subplots()
    
    ax.plot(cell_to_plot.V_tab, cell_to_plot.j_tab, linewidth = 1.5, color = "red")
    ax.set_xlim(0, max(cell_to_plot.V_tab) + 0.05)
    ax.set_ylim(0, max(cell_to_plot.j_tab) + 5)
    ax.set_xlabel('Voltage (V)')
    ax.set_ylabel('Current density (mA/cm2)')
    
    ax2 = ax.twinx()
    ax2.plot(cell_to_plot.V_tab, cell_to_plot.P_tab, linewidth = 1.5, color = "blue")
    ax2.set_xlim(0, max(cell_to_plot.V_tab) + 0.05)
    ax2.set_ylim(0, max(cell_to_plot.P_tab) + 5)
    ax2.set_ylabel('Power (W/m²)')
    
    plt.show()
    fig.savefig('res/current+power-voltage.png')
"""

"""
========================================
Efficiency or temperature vs Gap
========================================
"""
"""
Egmin = 0.5 # Gap min(eV)
Egmax = 3.1 # Gap max (eV)
Eg_tab = [] #gaps in (eV)
eff_tab = [] #efficiency tab (%)
Tc_tab = [] #cell temperature tab (K)
E = Egmin
dE = 0.03 #step

f = open('res.txt', 'w')

while E <= Egmax:
    Cell = thermal_correction(MJ = Multijunction(Eg_tab = [E], T = Ta, spectrum = spectrum), Ta = Ta, h = h, th_irradiance = th_irradiance, em = em)
    eff_tab.append(Cell.eff)
    Tc_tab.append(Cell.T)
    Eg_tab.append(E)
    f.write(f"{E}, {Cell.T} \n")
    E = E + dE

f.close()

# On trace la courbe
plt.plot(Eg_tab, Tc_tab, linewidth=1.5)
plt.xlim(Egmin, Egmax)
plt.ylim(min(Tc_tab)-10, max(Tc_tab)+10)
plt.xlabel('Band gap energy Eg (eV)')
plt.ylabel('Power conversion efficiency (%)')
plt.savefig('res/temperature-Eg.png')

print(Eg_tab)
print("")
print(Tc_tab)
print("")
print(eff_tab)
"""
