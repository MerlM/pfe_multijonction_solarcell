
******************   SMARTS, version 2.9.5   *******************

 Simple Model of the Atmospheric Radiative Transfer of Sunshine
     Chris A. Gueymard, Solar Consulting Services
                    December 2005

    This model is documented in FSEC Report PF-270-95
 and in a Solar Energy paper, vol. 71, No.5, 325-346 (2001)

 NOTE: These references describe v. 2.8 or earlier!!!
 See the User's Manual for details on the considerable 
 changes that followed...

****************************************************************

   Reference for this run: test                                                            

----------------------------------------------------------------

* ATMOSPHERE : USSA        AEROSOL TYPE: S&F_RURAL                                                       

* INPUTS:
     Pressure (mb) =  992.140   Ground Altitude (km) =   0.1800
     Height above ground (km) =   0.0000
     Relative Humidity (%) = 46.605   Precipitable Water (cm) =  1.3184
     Ozone (atm-cm) = 0.3433 or 343.3 Dobson Units
   AEROSOLS:  Optical Depth at 500 nm = 0.0840      Optical depth at 550 nm = 0.0764
       Angstrom's Beta = 0.0333       Schuepp's B = 0.0365
     Meteorological Range (km) =  124.2   Visibility (km) =   95.1
     Alpha1 = 0.9640  Alpha2 = 1.4313   Mean Angstrom's Alpha = 1.1977
     Season = SPRING/SUMMER           

* TEMPERATURES:
     Instantaneous at site's altitude = 287.0 K
     Daily average (reference) at site's altitude = 287.0 K
     Stratospheric Ozone and NO2 (effective) = 225.3 K

** WARNING #13 *********
\\ Ground reflectance data for USER_DEFINED            
\\ extend only from 0.3000 to 3.0000 �m,
\\ whereas the wavelength limits for this run are 0.2800 and 4.0000 �m.
\\ Consequently, reflectance is fixed at 0.002 below 0.3000 �m and at 0.000 above 3.0000 �m.



The following spectral variables will be output to file: smarts295.ext.txt       

 * Direct_tilted_irradiance


      Spectral ZONAL albedo data: USER_DEFINED            
      with a reflection process: LAMBERTIAN              




      Spectral LOCAL albedo data: LIGHT_SANDY_SOIL        
      with a reflection process: NON_LAMBERTIAN          



====================================================================================================
====================================================================================================

* SOLAR POSITION (deg.):
    Zenith Angle (apparent) = 83.876  Azimuth (from North) =  126.69

      RELATIVE OPTICAL MASSES:
  - Rayleigh =  8.692
  - Water Vapor =  9.188
  - Ozone =  7.554
  - NO2 =  7.424
  - Aerosols =  9.100

  Results below are for this specific day:
 Year = 2009   Month =  1  Day = 21   Hour (LST) =  9.000   Day of Year =  21
   In Universal Time:
   Day (UT) = 21   Hour (UT) =  8.000
   Julian Day =  2454852.750  Declination = -19.863 deg.  Radius vector = 0.98410   Equation of Time (min) = -11.279
   Local Apparent Time (or Solar Time):   8.134



 CO2 Mixing Ratio (ppmv):  370.0



 Total column abundances (atm-cm) for all gases except H2O, and for normal/standard conditions:

    BrO       CH2O        CH4      ClNO3         CO        CO2       HNO2       HNO3        NH3

0.2500E-05 0.3000E-03 0.1298E+01 0.1200E-03 0.8626E-01 0.2912E+03 0.1000E-03 0.3628E-03 0.1676E-03


     NO        NO2        NO3         N2        N2O         O2         O3         O4        SO2

0.3098E-03 0.2041E-03 0.5000E-04 0.3691E+06 0.2421E+00 0.1645E+06 0.3433E+00 0.1645E+06 0.1053E-03




* ANGLES (deg.) FOR TILTED SURFACE CALCULATIONS: 
   Surface Tilt =  30.000   Surface Azimuth (from North) = 180.000
   Incidence Angle =  67.083

  Diffuse irradiance ratios (tilted plane/horizontal):
      0.9330  (isotropic approximate conversion--for reference)
      1.5678  (anisotropic conversion model--used here)



*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *

** SPECTRUM:
   Total (0-100 �m) Extraterrestrial Irradiance used here = 1411.52 W/m2
  (i.e., 1.0326 times the selected solar constant, 1367.00 W/m2, due to the actual Sun-Earth distance.)
